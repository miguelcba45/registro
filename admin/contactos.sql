-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `contactos`;
CREATE TABLE `contactos` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(50) NOT NULL,
  `Direccion` varchar(50) NOT NULL,
  `Telefono` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `contactos` (`Id`, `Nombre`, `Direccion`, `Telefono`) VALUES
(1,	'Miguel',	'Tocutiyo',	'456466'),
(2,	'Arturo',	'Carabobo',	'54654654645');

-- 2017-02-24 03:19:12
