<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		$data['titulo'] = 'Agenda Web';

		$this->load->view('layout/header', $data);
		$this->load->view('home/index');
		$this->load->view('layout/footer');
	}

}

/* End of file Home.php */
/* Location: ./application/controllers/Home.php */