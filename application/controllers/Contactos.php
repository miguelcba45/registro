<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contactos extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('model_contactos');
	}

	public function index()
	{
		$data['titulo'] = 'Página Principal';
		$data['query'] = $this->model_contactos->getAll();

		$this->load->view('layout/header', $data);
		$this->load->view('contacto/contactos');
		$this->load->view('layout/footer');
	}

		public function Agregar()
	{
		$data['titulo'] = 'Agregar nuevo contacto';

		$this->load->view('layout/header', $data);
		$this->load->view('contacto/agregar');
		$this->load->view('layout/footer');
	}

		public function AgregarContacto()
		{
			$this->form_validation->set_rules('nnombre','Nombre','required');
			$this->form_validation->set_rules('ndireccion','Direccion','required');
			$this->form_validation->set_rules('ntelefono','Telefono','required');

			if($this->form_validation->run() == FALSE) {
				//ERROR
				$data['titulo'] = 'Agregar nuevo contacto';
				$this->load->view('layout/header', $data);
				$this->load->view('contacto/agregar');
				$this->load->view('layout/footer');
					} 
					else 
					{
				$data = array(
					'Nombre'=>$this->input->post('nnombre'),
					'Direccion'=>$this->input->post('ndireccion'),
					'Telefono'=>$this->input->post('ntelefono'),
					);
					$this->model_contactos->insertar($data);

					redirect( base_url() . 'contactos/');
				}

		}

}

/* End of file principal.php */
/* Location: ./application/controllers/principal.php */